<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task03_uml_class_diagram.png
[result_image]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task03_result.png

<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/

<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task03/Demo.java
[View]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task03/View.java
[ViewResult]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task03/ViewResult.java
[Viewable]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task03/Viewable.java
[ViewableResult]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task03/ViewableResult.java

<!-- Tests -->
[DemoTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task03/DemoTest.java

# СПАДКУВАННЯ. ІНТЕРФЕЙСИ. КОЛЕКЦІЇ ПАКЕТУ java.util

### Лабораторна робота №3

### Мета:
- набуття практичних навичок розробки в сереовищі *Eclipse* з використанням основних принципів ООП;
- вивчення методів організації колекцій об'єктів засобами пакету `java.util`;
- використання шаблону проектування *Factory Method*;
- використання модульного тестування.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
У якості основи використовувати початковий текст проекту  попередньої лабораторної роботи. Забезпечити розміщення результатів обчислень у колекції з можливістю збереження/відновлення.  
Використовуючи шаблон проектування `Factory Method` розробити ієрархію, передбачаючу розширення за рахунок додавання нових класів відображення.  
Розширити ієрархію інтерфесом "фабрикуємих" об'єктів, що представляють набір методів для виводу результатів у текстовому відображенні. розробити і реалізувати інтерфейс для "фабрикуючого" метода.  
Забезпечити діалоговий інтерфейс з користувачем.  
Розробити клас для тестування основного функціоналу.  
Використовувати коментарі для автоматичної генерації документації засобами `Javadoc`.  

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Calculates and show results. |
| [View.java][View] | Product. Interface of "factoried" objects. |
| [ViewResult.java][ViewResult] | ConcreteProduct. Calculates tetrades, saves and shows results. |
| [Viewable.java][Viewable] | Creator. Declares method, which "factory" objects. |
| [ViewableResult.java][ViewableResult] | ConcreteCreator. Implements method "factoring" objects. |

#### 2.3.2 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [DemoTest.java][DemoTest] | Tests developed classes. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image]

# ВИСНОВКИ
В розробленій програмі було реалізовано шаблон проектування `Factory Method` за допомогою інтерфейсів `View` та `Viewable`, а також класів `ViewResult` та `VieableResult`. Для демонстрації роботи програми був розроблений клас `Demo`. Для тестування базового функціоналу був розроблений клас `DemoTest`.
