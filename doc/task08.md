<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task08_uml_class_diagram.png
[result_image_1]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task08_result_1.png
[result_image_2]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task08_result_2.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task08/Demo.java
[ViewableWindow]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task08/ViewableWindow.java
[ViewWindow]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task08/ViewWindow.java
[Window]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task08/Window.java

# ВИКОРИСТАННЯ ГРАФІЧНИХ ПРИМІТИВІВ. WINDOWS TOOLKIT

### Лабораторна робота №8

### Мета:
- ознайомлення з інструментарієм `Abstract Windows Toolkit`;
- реалізація відображення результатів обробки колекції об'єктів в графічному вигляді;
- використання шаблону проектування `Factory Method` при розробці класів відображення графіка;
- підготовка документації на основі коментарів інструментом автоматичної генерації `javadoc`.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
Используя ранее созданные классы, разработать приложение, отображающее результаты обработки коллекции объектов в графическом виде (график функции, диаграмма и т.д.). При разработке классов использовать
шаблон `Factory Method` (Virtual Constructor) или `Observer`.  
Обеспечить диалоговый интерфейс с пользователем и перерисовку графика при изменении значений элементов коллекции.  
Использовать комментарии для автоматической генерации документации средствами `javadoc`.  

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Runs application. |
| [ViewableWindow.java][ViewableWindow] | Creates ViewWindow object. |
| [ViewWindow.java][ViewWindow] | Show diagram. |
| [Window.java][Window] | Creates window. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image_1]  
![Result of program work][result_image_2]

# ВИСНОВКИ
У даній лабораторній роботі було реалізовано відображення результатів обробки колекції в графічному вигляду за допомогою засобів `Abstract Windows Toolkit`. Було використано шаблон проектування `Factory Method`
