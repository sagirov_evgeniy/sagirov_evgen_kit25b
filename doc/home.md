### Звіти та проекти до лабораторних робіт з Java:

| № | Тема | Звіт |
| --------------------- | ---- | ---- |
| 1 | Ознайомлення з системою контролю версій Git | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task01) |
| 2 | Класи та об'єкти. Агрегування. Серіалізація | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task02) |
| 3 | Спадкування. Інтерфейси. Колекції пакету java.util | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task03) |
| 4 | Поліморфізм. Форматований вивід | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task04) |
| 5 | Обробка колекцій. Шаблони Singletone, Command | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task05) |
| 6 | Паралельне виконання. Шаблон Worker Thread | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task06) |
| 7 | Анотація. Рефлексія. Шаблон Observer | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task07) |
| 8 | Використання графічних примытивів. Abstract Windows Toolkit | [Перейти до звіту](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/wikis/task08) |
