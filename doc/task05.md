<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task05_uml_class_diagram.png
[result_image_1]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task05_result_1.png
[result_image_2]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task05_result_2.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Demo.java
[Application]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Application.java
[Menu]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Menu.java
[ChangeParameterCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/ChangeParameterCommand.java
[ChangeConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/ChangeConsoleCommand.java
[ChangeConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/ChangeConsoleCommand.java
[GenerateConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/GenerateConsoleCommand.java
[RestoreConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/RestoreConsoleCommand.java
[SaveConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/SaveConsoleCommand.java
[UndoConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/UndoConsoleCommand.java
[ViewConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/ViewConsoleCommand.java
[ParameterStateManager]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/ParameterStateManager.java
<!-- Interfaces -->
[Command]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Command.java
[ConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Undoable.java
[Undoable]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task05/Undoable.java
<!-- Tests -->
[AllTests]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task05/AllTests.java
[ChangeParameterCommandTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task05/ChangeParameterCommandTest.java
[UndoConsoleCommandTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task05/UndoConsoleCommandTest.java

# ОБРОБКА КОЛЕКЦІЙ.ШАБЛОНИ SINGLETONE, COMMAND

### Лабораторна робота №5

### Мета:
- реалізація алгоритмів обробки колекції об'єктів;
- демонстрація можливості відокремлення об'єктів, що оброблюються, від методів обробки на прикладі реалізації алгоритмів обробки колекції об'єктів;
- використання шаблонів проектування `Command`, `Singleton` и `Factory Method`;
- використання модульного тестування.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
Используя созданные ранее классы и шаблон проектирования `Command`, разработать класс Menu как расширяемый контейнер команд, реализовать обработку данных коллекции и отдельных элементов.  
Реализовать возможность отмены (undo) операций (команд).  
Продемонстрировать понятие "макрокоманда".  
При разработке приложения использовать шаблон `Singletone`.  
Обеспечить диалоговый интерфейс с пользователем.  
Разработать класс для тестирования функциональности приложения.  
Использовать комментарии для автоматической генерации документации средствами `javadoc`.

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Runs application. |
| [Application.java][Application] | Adds console commands to menu and runs it. |
| [Menu.java][Menu] | Handles console commands. Macrocommand. |
| [ChangeParameterCommand.java][ChangeParameterCommand] | Scales parameters. |
| [ChangeConsoleCommand.java][ChangeConsoleCommand] | Console command to scale parameters. |
| [GenerateConsoleCommand.java][GenerateConsoleCommand] | Console command to randomly generate parameters. |
| [RestoreConsoleCommand.java][RestoreConsoleCommand] | Console command to deserialize parameters. |
| [SaveConsoleCommand.java][SaveConsoleCommand] | Console command to serialize parameters. |
| [UndoConsoleCommand.java][UndoConsoleCommand] | Console command to undo last action. |
| [ViewConsoleCommand.java][ViewConsoleCommand] | Console command to show current state of parameters. |
| [ParameterStateManager.java][ParameterStateManager] | Storage for parameters states history. |

#### 2.3.2 Інтерфейси
| Ім'я | Призначення|
| ---- | ---------- |
| [Command.java][Command] | Sets behaviour for commands. |
| [ConsoleCommand.java][ConsoleCommand] | Sets behaviour for console commands. |
| [Undoable.java][Undoable] | Set behaviour for undoable actions. |

#### 2.3.3 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [AllTests.java][AllTests] | Runner for all tests. |
| [ChangeParameterCommandTest.java][ChangeParameterCommandTest] | Tests ChangeParameterCommand. |
| [UndoConsoleCommandTest.java][UndoConsoleCommandTest] | Tests UndoConsoleCommand. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image_1]  
![Result of program work][result_image_2]

# ВИСНОВКИ
В розробленій програмі було реалізовано алгоритми обробки колекцій та продемонстровано використання шаблонів проектування `Command`, `Singletone`, `Factory Method`.

