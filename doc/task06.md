<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task06_uml_class_diagram.png
[result_image_1]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task06_result_1.png
[result_image_2]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task06_result_2.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/Demo.java
[CommandQueue]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/CommandQueue.java
[ExecuteConsoleCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/ExecuteConsoleCommand.java
[AvgCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/AvgCommand.java
[MaxCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/MaxCommand.java
[MinMaxCommand]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/MinMaxCommand.java
<!-- Interfaces -->
[Queue]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task06/Queue.java
<!-- Tests -->
[AllTests]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task06/AllTests.java
[AvgCommandTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task06/AvgCommandTest.java
[MaxCommandTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task06/MaxCommandTest.java
[MinMaxCommandTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task06/MinMaxCommandTest.java
[CommandQueueTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task06/CommandQueueTest.java

# ПАРАЛЕЛЬНЕ ВИКОНАННЯ. ШАБЛОН WORKER THREAD

### Лабораторна робота №6

### Мета:
- ознайомлення з методами управління задачами, що виконуються паралельно;
- реалізація та демонстрація механізму обслуговування черги задач;
- використання шаблонів `Worker Thread`, `Command` і `Factory Method` для реалізації паралельного виконання обробки колекції об'єктів;
- використання модульного тестування.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
Продемонстрировать возможность параллельной обработки элементов коллекции (поиск минимума, максимума, вычисление среднего значения, отбор по критерию, статистическая обработка и т.д.).  
Управление очередью задач (команд) реализовать с помощью шаблона Worker Thread.  
Обеспечить диалоговый интерфейс с пользователем.  
Разработать класс для тестирования функциональности приложения.  
Использовать комментарии для автоматической генерации документации средствами javadoc.

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Runs application. |
| [CommandQueue.java][CommandQueue] | Command queue for threads handling. |
| [ExecuteConsoleCommand.java][ExecuteConsoleCommand] | Console command to execute all calculating algorythms. |
| [MaxCommand.java][MaxCommand] | Command to find maximum. |
| [AvgCommand.java][AvgCommand] | Command to find average. |
| [MinMaxCommand.java][MinMaxCommand] | Command to find minimum and maximum. |

#### 2.3.2 Інтерфейси
| Ім'я | Призначення|
| ---- | ---------- |
| [Queue.java][Queue] | Sets behaviour for queue. |

#### 2.3.3 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [AllTests.java][AllTests] | Runner for all tests. |
| [CommandQueueTest.java][CommandQueueTest] | Tests CommandQueue. |
| [MaxCommandTest.java][MaxCommandTest] | Tests MaxCommand. |
| [AvgCommandTest.java][AvgCommandTest] | Tests AvgCommand. |
| [MinMaxCommandTest.java][MinMaxCommandTest] | Tests MinMaxCommand. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image_1]  
![Result of program work][result_image_2]

# ВИСНОВКИ
В розробленій програмі було реалізовано алгоритми обробки колекцій та продемонстровано використання шаблонів проектування `Command`, `Worker Thread`, `Factory Method`.

