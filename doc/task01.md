<!-- Images -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task01_uml_class_diagram.png
[img1]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img1.png
[img2]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img2.png
[img3]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img3.png
[img4]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img4.png
[img5]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img5.png
[img6]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img6.png
[img7]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img7.png
[img8]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img8.png
[img9]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task01/img9.png
<!-- Links -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
[class_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task01

# ОЗНАЙОМЛЕННЯ З СИСТЕМОЮ КОНТРОЛЮ ВЕРСІЙ GIT

### Лабораторна робота №1

### Мета:
- придбання та закріплення навичок створення та використання репозиторіїв Git;
- розробки найпростіших Java програм з використанням сховища Git;
- ознайомлення з можливостями середовища розробки Eclipse та його розширення EGit.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
1.1 Підготувати сховище для розміщення проекту.  
1.2 Виконати розробку найпростішої консольної програми на `Java` та розмістити її у локальному сховищі.  
1.3 У персональному кабінеті *gitlab.com* отримати адресу доступу до видаленого сховища по протоколу `HTTPS`.  
1.4 Продовжити розробку програми у іншому каталозі.  
1.5 Виконати розробку `Java` проекту у `Eclipse` з розміщенням у видаленому сховищі.  

# 2 ХІД ВИКОНАННЯ
2.1 Додавання .gitignore  
![img1][img1]  
2.2 Текст файлу `Main.java`  
[Main.java](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/9a01584c4f3ab6c591f4f9418688099a76738322/Task01/Main.java)  
2.3 Запуск програми з аргументами  
![img2][img2]  
2.4 Генерація документації за допомогою `Javadoc`  
![img3][img3]  
2.5 Документація створена за допомогою `Javadoc`  
![img4][img4]  
2.6 Клонування сховища  
![img5][img5]  
2.7 Додавання коментарів  
[Main.java](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/85ab798875ea2663356a5feb6898bfcbffca4c71/Task01/Main.java)  
2.8 Комміт з клонованого репозиторія  
![img6][img6]  
2.9 Зміст сховища  
[Кореневий каталог](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/85ab798875ea2663356a5feb6898bfcbffca4c71)  
2.10 Історія комітів  
[Commits](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/commits/68750df89d6d4e5a3d078251b953c2cc8b60611a)  
2.11 Структура сховища відображена у *EGit*  
![img7][img7]  
2.12 Прикріплення сховища до проекту  
![img8][img8]  
2.13 Результат запуску програми у *Eclipse*  
![img9][img9]  
2.14 Структура проекту  
[Кореневий каталог](https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/68750df89d6d4e5a3d078251b953c2cc8b60611a)  
2.15 UML діаграма класів  
![UML][UML_class_diagram]  

# ВИСНОВКИ
Було придбано та закріплено навички створення та використання репозиторіїв *Git*.
