<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task04_uml_class_diagram.png
[result_image]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task04_result.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task04/Demo.java
[ViewTable]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task04/ViewTable.java
[ViewableTable]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task04/ViewableTable.java
<!-- Tests -->
[DemoTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task04/DemoTest.java

# ПОЛІМОРФІЗМ. ФРМАТОВАНИЙ ВИВІД

### Лабораторна робота №4

### Мета:
- набуття практичних навичок розробки програм з використанням поліморфізму;
- вивчення особливостей перевантаження і перевизначення методів;
- ознайомлення із засобами форматованого виведення тексту до консолі;
- використання шаблону проектування `Factory Method`;
- використання модульного тестування.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
У якості основи використовувати початковий код проекту попередньої лабораторної роботи. Використовуючи шаблон проектування `Factory Method`, розширити ієрархію довільними класами, реалізуючими методи для представлення результатів у вигляді текстової таблиці. Параметри відображення таблиці повинні визначатись користувачем.  
Продемонструвати заміщення (перевизначення), суміщення (перевантаження), динамічне призначення методів.  
Забезпечити діалоговой інтерфейс з користувачем.  
Розробити клас для тестування основної функціональності.  
Використовувати коментарі для автоматичної генерації документації засобами `javadoc`.

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Calculates and show results. |
| [ViewTable.java][ViewTable] | ConcreteProduct.Shows data in table format. |
| [ViewableTable.java][ViewableTable] | ConcreteCreator. |

#### 2.3.2 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [DemoTest.java][DemoTest] | Tests developed classes. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image]

# ВИСНОВКИ
В розробленій програмі було розширено шаблон проектування `Factory Method` за допомогою `ViewableTable` та `ViewTable`. Для демонстрації роботи програми був розроблений клас `Demo`. Для тестування базового функціоналу був розроблений клас `DemoTest`.
