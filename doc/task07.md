<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task07_uml_class_diagram.png
[result_image]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task07_result.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/Demo.java
[Item]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/Item.java
[Items]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/Items.java
[ItemsGenerator]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/ItemsGenerator.java
[ItemsSorter]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/ItemsSorter.java
[Observable]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/Observable.java
[AnnotatedObserver]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/AnnotatedObserver.java
<!-- Interfaces -->
[Observer]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task07/Observer.java
<!-- Tests -->
[DemoTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task07/DemoTest.java

# АНОТАЦІЇ. РЕФЛЕКСІЯ. ШАБЛОН OBSERVER

### Лабораторна робота №7

### Мета:
- придбання навичок використання засобів анотування;
- ознайомлення з механізмом рефлексії;
- реалізація обслуговування колекції об'єктів на основі шаблону проектування `Observer`;
- використання модульного тестування;
- підготовка документації на основі коментарів інструментом автоматичної генерації `javadoc`.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ
Разработать иерархию классов согласно шаблону Observer и продемонстрировать возможность обслуживания разработанной ранее коллекции (наблюдаемый объект, Observable) различными (не менее двух) наблюдателями (Observers) – отслеживание изменений, упорядочивание, вывод, отображение и т.д. При реализации иерархии классов использовать средства аннотирования (Annotation). Отметить особенности различных политик удержания аннотаций (annotation retention policies). Продемонстрировать поддержку классами концепции рефлексии (Reflection).  
Разработать класс для тестирования функциональности приложения.  
Использовать комментарии для автоматической генерации документации средствами javadoc.  

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [AnnotatedObserver.java][AnnotatedObserver] | Superclass for observer, uses annotation to recognize methods. |
| [Demo.java][Demo] | Runs application. |
| [Item.java][Item] | Collection element. |
| [Items.java][Items] | Collection. |
| [ItemsGenerator.java][ItemsGenerator] | Observer. |
| [ItemsSorter.java][ItemsSorter] | Observer. |
| [Observable.java][Observable] | Shows methods of connection between observer and abservable objects. |

#### 2.3.2 Інтерфейси
| Ім'я | Призначення|
| ---- | ---------- |
| [Observer.java][Observer] | Sets behaviour for observers. |

#### 2.3.3 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [DemoTest.java][DemoTest] | Tests developed program. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image]

# ВИСНОВКИ
У данній лабораторній роботі було реалізовано обробку колекції на основі шаблону проектування `Observer`.
