<!-- Images  -->
[UML_class_diagram]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/sagirov_evgeniy/task02_uml_class_diagram.png
[result_image]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/raw/master/img/task02_result.png
<!-- Project folder -->
[project_folder]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/tree/master/sagirov_evgeniy/
<!-- Classes -->
[Demo]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task02/Demo.java
[Parameters]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task02/Parameters.java
[Solver]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/src/ua/khpi/sagirov_evgeniy/task02/Solver.java
<!-- Tests -->
[DemoTest]: https://gitlab.com/khpi-se-kit25b/sagirov_evgeniy/blob/master/sagirov_evgeniy/test/ua/khpi/sagirov_evgeniy/task02/DemoTest.java

# КЛАСИ ТА ОБ'ЄКТИ. АГРЕГУВАННЯ. СЕРІАЛІЗАЦІЯ

### Лабораторна робота №2

### Мета:
- демонстрація принципів агрегування при розробці класів;
- використання можливості серіалізації об'єктів;
- ознайомлення з середовищем розробки Eclipse;
- набуття практичних навичок створення і налагодження програми мовою Java на прикладі простого проекту.

# 1 ІНДИВІДУАЛЬНЕ ЗАВДАННЯ

Розробити клас, що серіалізується для зберігання параметрів і результатів обчислень.  
Використовуючи агрегацію, розробити клас для знаходження рішення задачі.  
Розробити клас для демонстрації в діалоговому режимі збереження та відновлення стану об'єкта, використовуючи серіалізацію. Показати особливості використання `transient` полів.  
Розробити клас для тестування коректності результатів обчислень та серіалізації/десеріалізації.  
Використовувати докладні коментарі для автоматичної генерації документації засобами `javadoc`.  
Прикладна задача: підрахувати кількість повних тетрад у двійковому представленні десткового числа.

# 2 РОЗРОБКА ПРОГРАМИ

### 2.1 Ієрархія та структура класів
![UML class diagram][UML_class_diagram]  

### 2.2 Опис програми
Детальний опис програми наведено у коментарях до початкового коду класів [проекту][project_folder].  

### 2.3 Текст програми
#### 2.3.1 Класи програми
| Ім'я | Призначення|
| ---- | ---------- |
| [Demo.java][Demo] | Demonstrates serialization/deserialization of object. |
| [Parameters.java][Parameters] | Stores parameters of calculations and it's result. |
| [Solver.java][Solver] | Solves task main objective. |

#### 2.3.2 Класи тестів
| Ім'я | Призначення|
| ---- | ---------- |
| [DemoTest.java][DemoTest] | Tests developed classes. |

# 3 РЕЗУЛЬТАТИ РОБОТИ
![Result of program work][result_image]

# ВИСНОВКИ
В розробленій програмі було реалізовано класи `Parameters`, `Demo`, `Solver`, які вирішують індивідуальне завдання та демонструють роботу  діалоговому режимі. Також було реалізовано `DemoTest`, що тестує правильність виконная основних задач програми. 
