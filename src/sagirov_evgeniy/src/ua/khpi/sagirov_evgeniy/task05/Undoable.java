package ua.khpi.sagirov_evgeniy.task05;

/**
 * Undoable interface.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface Undoable {
    /**
     * Saves state to history.
     */
    void saveState();
}