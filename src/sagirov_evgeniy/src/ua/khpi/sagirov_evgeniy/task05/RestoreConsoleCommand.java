package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * @author sagirov evgeniy
 * @version 1.0
 */
public class RestoreConsoleCommand implements Undoable, ConsoleCommand {
    /** Object implementing {@linkplain View}. */
    private final View view;

    /**
     * Constructor.
     *
     * @param aView new value for {@linkplain RestoreConsoleCommand#view}
     */
    public RestoreConsoleCommand(final View aView) {
	view = aView;
    }

    @Override
    public final void execute() {
	saveState();
	System.out.println("Restore last saved.");
	try {
	    view.viewRestore();
	} catch (final Exception e) {
	    System.err.println("Deserialization error: " + e);
	}
	view.viewShow();
    }

    @Override
    public final char getKey() {
	return 'r';
    }

    @Override
    public final String toString() {
	return "'r'estore";
    }

    @Override
    public void saveState() {
	ParameterStateManager psm = ParameterStateManager.getInstance();
	psm.addToHistory(view);
    }
}