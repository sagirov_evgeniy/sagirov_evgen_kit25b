package ua.khpi.sagirov_evgeniy.task05;

import java.io.IOException;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * @author sagirov evgeniy
 * @version 1.0
 */
public class SaveConsoleCommand implements ConsoleCommand {
    /** Object implementing {@linkplain View}. */
    private final View view;

    /**
     * Constructor.
     *
     * @param aView new value for {@linkplain SaveConsoleCommand#view}
     */
    public SaveConsoleCommand(final View aView) {
	view = aView;
    }

    @Override
    public final void execute() {
	System.out.println("Save current.");
	try {
	    view.viewSave();
	} catch (final IOException e) {
	    System.err.println("Serialization error: " + e);
	}
	view.viewShow();
    }

    @Override
    public final char getKey() {
	return 's';
    }

    @Override
    public final String toString() {
	return "'s'ave";
    }
}