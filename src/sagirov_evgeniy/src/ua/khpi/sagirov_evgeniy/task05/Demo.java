package ua.khpi.sagirov_evgeniy.task05;

/**
 * Calculates and shows results.
 *
 * @author sagirov evgeniy
 * @version 1.0
 * @see Demo#main
 */
public final class Demo {

    /**
     * Entry point of console program.
     *
     * @param args list of arguments
     */
    public static void main(final String[] args) {
	Application app = Application.getInstance();
	app.run();
    }

    /** Constructor. */
    private Demo() {
    }
}