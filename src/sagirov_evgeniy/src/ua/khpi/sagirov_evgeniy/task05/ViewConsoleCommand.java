package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * View console command.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ViewConsoleCommand implements ConsoleCommand {

    /** {@linkplain View} object. */
    private final View view;

    /**
     * Constructor.
     *
     * @param aView sets
     */
    public ViewConsoleCommand(final View aView) {
	view = aView;
    }

    @Override
    public void execute() {
	System.out.println("View current.");
	view.viewShow();
    }

    @Override
    public final char getKey() {
	return 'v';
    }

    @Override
    public String toString() {
	return "'v'iew";
    }
}