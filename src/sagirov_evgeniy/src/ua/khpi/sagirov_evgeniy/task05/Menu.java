package ua.khpi.sagirov_evgeniy.task05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Macrocommand (Command pattern). Collection of ConsoleCommands
 *
 * @author sagirov evgeniy
 * @version 1.0
 * @see ConsoleCommand
 */
public class Menu implements Command {

    /**
     * Collection of console commands.
     *
     * @see ConsoleCommand
     */
    private final List<ConsoleCommand> menu = new ArrayList<ConsoleCommand>();

    /**
     * Adds new command to collection.
     *
     * @param command implements {@linkplain ConsoleCommand}
     * @return command
     */
    public final ConsoleCommand add(final ConsoleCommand command) {
	menu.add(command);
	return command;
    }

    @Override
    public final void execute() {
	String s = null;
	final BufferedReader in = new BufferedReader(
		new InputStreamReader(System.in));
	menu: while (true) {
	    do {
		System.out.println(this);
		try {
		    s = in.readLine();
		} catch (final IOException e) {
		    System.err.println("Error: " + e);
		    System.exit(0);
		}
	    } while (s.length() != 1);
	    final char key = s.charAt(0);
	    if (key == 'q') {
		System.out.println("Exit.");
		break menu;
	    }
	    for (final ConsoleCommand c : menu) {
		if (s.charAt(0) == c.getKey()) {
		    c.execute();
		    continue menu;
		}
	    }
	    System.out.println("Wrong comand.");
	    continue menu;
	}
    }

    @Override
    public final String toString() {
	String s = "Enter command...\n";
	for (final ConsoleCommand c : menu) {
	    s += c + ", ";
	}
	s += "'q'uit: ";
	return s;

    }
}