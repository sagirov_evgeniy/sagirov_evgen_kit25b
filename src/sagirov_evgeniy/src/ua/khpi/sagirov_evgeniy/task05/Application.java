package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task04.ViewableTable;
import ua.khpi.sagirov_evgeniy.task08.ViewableWindow;

/**
 * Genereates and shows menu; implements Singleton patter.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public final class Application {

    /**
     * Reference to Application object; Singleton pattern.
     *
     * @see Application
     */
    private static Application instance = null;

    /**
     * Return reference to Application object; Singleton pattern.
     *
     * @return {@linkplain Application#instance}
     */
    public static Application getInstance() {
	if (instance == null) {
	    return new Application();
	}
	return instance;
    }

    /**
     * Object implementing {@linkplain View} interface; serves collection of
     * {@linkplain ua.khpi.sagirov_evgeniy.task02.Parameters}; initializes due
     * to Factory Method.
     */
    private View view;

    /** {@linkplain Menu} object; macrocommand (Command pattern). */
    private final Menu menu = new Menu();

    /** Unreachabe constructor; Singleton pattern. */
    private Application() {
    }

    /**
     * User commands handling.
     *
     * @see Application
     */
    public final void run() {
	this.view = new ViewableTable().getView();
	menu.add(new ViewConsoleCommand(view));
	menu.add(new GenerateConsoleCommand(view));
	menu.add(new ChangeConsoleCommand(view));
	menu.add(new SaveConsoleCommand(view));
	menu.add(new RestoreConsoleCommand(view));
	menu.add(new UndoConsoleCommand(view));
	menu.execute();
    }

    public void run(View aView) {
	this.view = aView;
	menu.add(new ViewConsoleCommand(view));
	menu.add(new GenerateConsoleCommand(view));
	menu.add(new ChangeConsoleCommand(view));
	menu.add(new SaveConsoleCommand(view));
	menu.add(new RestoreConsoleCommand(view));
	menu.add(new UndoConsoleCommand(view));
	menu.execute();
    }
}
