package ua.khpi.sagirov_evgeniy.task05;

/**
 * Console command.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface ConsoleCommand extends Command {

    /**
     * Hotkey of command; Command pattern.
     *
     * @return hotkey symbol
     */
    char getKey();
}