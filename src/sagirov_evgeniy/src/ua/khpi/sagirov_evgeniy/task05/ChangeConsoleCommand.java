package ua.khpi.sagirov_evgeniy.task05;

import java.util.Random;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;

/**
 * Console Change item command.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ChangeConsoleCommand extends ChangeParameterCommand
	implements Undoable, ConsoleCommand {

    /** {@linkplain View} object. */
    private View view;

    /**
     * Constructor.
     * 
     * @param aView value for {@linkplain ChangeConsoleCommand#view}
     */
    public ChangeConsoleCommand(final View aView) {
	this.view = aView;
    }

    @Override
    public final void execute() {
	saveState();
	Random rnd = new Random();
	final int maxRandomValue = 50;
	System.out.println("Change item: scale factor "
		+ setOffset(rnd.nextInt(maxRandomValue)));
	for (Parameters param : ((ViewResult) view).getParams()) {
	    super.setParam(param);
	    super.execute();
	}
	view.viewSolve();
	view.viewShow();
    }

    @Override
    public final char getKey() {
	return 'c';
    }

    /**
     * Gets the value of {@linkplain ChangeConsoleCommand#view}.
     * 
     * @return value of {@linkplain ChangeConsoleCommand#view}
     */
    public final View getView() {
	return view;
    }

    /**
     * Sets the value of {@linkplain ChangeConsoleCommand#view}.
     * 
     * @param aView new value for {@linkplai ChangeConsoleCommand#view}
     * @return {@linkplain ChangeConsoleCommand#view}
     */
    public final View setView(final View aView) {
	this.view = aView;
	return view;
    }

    @Override
    public final String toString() {
	return "'c'hange";
    }

    @Override
    public void saveState() {
	ParameterStateManager psm = ParameterStateManager.getInstance();
	psm.addToHistory(view);
    }
}