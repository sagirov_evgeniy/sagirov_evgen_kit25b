package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task02.Parameters;

/**
 * Change item command; Command pattern.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ChangeParameterCommand implements Command {

    /** Handling object; Command pattern. */
    private Parameters param;

    /** Command preference; Command pattern. */
    private int offset;

    @Override
    public void execute() {
	param.setNumber(param.getNumber() * offset);
    }

    /**
     * @return the offset
     */
    public final int getOffset() {
	return offset;
    }

    /**
     * Gets {@linkplain ChangeItemCommand#param} value.
     *
     * @return value of {@linkplain ChangeItemCommand#param}
     */
    public final Parameters getParam() {
	return param;
    }

    /**
     * @param aOffset new value of {@linkplain ChangeItemCommand#offset}
     * @return offset
     */
    public final int setOffset(final int aOffset) {
	offset = aOffset;
	return offset;
    }

    /**
     * Sets the {@linkplain ChangeItemCommand#param} field.
     *
     * @param aParam new value of {@linkplain ChangeItemCommand#param}
     * @return param
     */
    public final Parameters setParam(final Parameters aParam) {
	param = aParam;
	return param;
    }
}