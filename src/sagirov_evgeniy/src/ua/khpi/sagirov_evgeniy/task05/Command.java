package ua.khpi.sagirov_evgeniy.task05;

/**
 * Command or task interface; patterns: Command, Worker Thread.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface Command {

    /** Runs command; patterns: Command, Worker Thread. */
    void execute();
}