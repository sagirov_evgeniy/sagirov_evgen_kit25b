package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * Undo command.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class UndoConsoleCommand implements ConsoleCommand {

    /** Object implementing {@linkplain View}. */
    private View view;

    /**
     * Constructor.
     * 
     * @param aView
     */
    public UndoConsoleCommand(View aView) {
	this.view = aView;
    }

    @Override
    public void execute() {
	if (view.viewUndo()) {
	    System.out.println("Undoing last action:");
	    view.viewShow();
	}
    }

    @Override
    public char getKey() {
	return 'u';
    }

    @Override
    public String toString() {
	return "'u'ndo";
    }
}