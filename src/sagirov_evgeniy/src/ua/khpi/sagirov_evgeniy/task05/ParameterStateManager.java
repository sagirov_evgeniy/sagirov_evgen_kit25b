package ua.khpi.sagirov_evgeniy.task05;

import java.util.ArrayList;
import java.util.List;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;

/**
 * Command manager to handle undo commands.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public final class ParameterStateManager {

    /** Instance of object. */
    private static ParameterStateManager instance = null;

    /** History of commands. */
    private static List<ArrayList<Parameters>> history = new ArrayList<ArrayList<Parameters>>();

    /**
     * Gets the value of {@linkplain ParameterStateManager#history}.
     * 
     * @return value of {@linkplain ParameterStateManager#history}
     */
    public int getLength() {
	return history.size();
    }

    /**
     * Constructor.
     */
    private ParameterStateManager() {
    }

    /**
     * Gets the value of {@linkplain ParameterStateManager#instance}.
     * 
     * @return value of {@linkplain ParameterStateManager#instance}
     */
    public static ParameterStateManager getInstance() {
	if (instance == null) {
	    return new ParameterStateManager();
	}
	return instance;
    }

    /**
     * Undo.
     * 
     * @return list of parameters
     */
    public ArrayList<Parameters> getPrevious() {
	return history.remove(history.size() - 1);
    }

    /**
     * Adds current state of parameters to history.
     * 
     * @param view {@linkplain View} object
     */
    public void addToHistory(final View view) {
	ArrayList<Parameters> temp = new ArrayList<Parameters>();
	for (Parameters p : ((ViewResult) view).getParams()) {
	    temp.add(new Parameters(p));
	}
	history.add(temp);
    }
}