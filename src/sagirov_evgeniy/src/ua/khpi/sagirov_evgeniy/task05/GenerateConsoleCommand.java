package ua.khpi.sagirov_evgeniy.task05;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * Generating command. Command pattern.
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public class GenerateConsoleCommand implements Undoable, ConsoleCommand {

    /** Object implementing {@linkplain View}. */
    private final View view;

    /**
     * Constructor.
     *
     * @param aView new value for {@linkplain SaveConsoleCommand#view}
     */
    public GenerateConsoleCommand(final View aView) {
	view = aView;
    }

    @Override
    public final void execute() {
	saveState();
	System.out.println("Random generation.");
	view.viewInit();
	view.viewShow();
    }

    @Override
    public final char getKey() {
	return 'g';
    }

    @Override
    public final String toString() {
	return "'g'enerate";
    }

    @Override
    public void saveState() {
	ParameterStateManager psm = ParameterStateManager.getInstance();
	psm.addToHistory(view);
    }
}