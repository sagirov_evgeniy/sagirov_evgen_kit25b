package ua.khpi.sagirov_evgeniy.task08;

import ua.khpi.sagirov_evgeniy.task05.Application;

/**
 * Calculates and showsresults.
 * 
 * @author sagirov evgeniy
 * @version 7.0
 */
public final class Demo {

    /** Hidden constructor. */
    private Demo() {
    }

    /**
     * Entry point of console program.
     * 
     * @param args array of incoming arguments
     */
    public static void main(final String[] args) {
	Application app = Application.getInstance();
	app.run(new ViewableWindow().getView());
	System.exit(0);
    }
}