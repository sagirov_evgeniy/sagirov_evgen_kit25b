package ua.khpi.sagirov_evgeniy.task08;

import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.Viewable;

/**
 * Creates ViewWindow object.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ViewableWindow implements Viewable {

    @Override
    public final View getView() {
	return new ViewWindow();
    }
}