package ua.khpi.sagirov_evgeniy.task08;

import java.awt.Dimension;

import ua.khpi.sagirov_evgeniy.task03.ViewResult;

/**
 * Show diagram.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ViewWindow extends ViewResult {

    /** Collection size. */
    private static final int POINTS_NUM = 100;

    /** Viewable window. */
    private Window window = null;

    /** Constructor. Creating of window. */
    public ViewWindow() {
	super(POINTS_NUM);
	super.init(50);
	window = new Window(this);
	window.setSize(new Dimension(640, 480));
	window.setTitle("Results");
	window.setVisible(true);
    }

    @Override
    public final void viewShow() {
	super.viewShow();
	window.setVisible(true);
	window.repaint();
    }
}