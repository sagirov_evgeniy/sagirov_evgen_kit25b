package ua.khpi.sagirov_evgeniy.task08;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.WindowAdapter;

import com.sun.glass.events.WindowEvent;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;

/**
 * Creates window.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class Window extends Frame {
    /** Border spacing. */
    private static final int BORDER = 20;

    /** Serves collection. */
    private ViewResult view;

    /**
     * Creates window event handler.
     * 
     * @param aView value for {@linkplain Window#view}
     */
    public Window(final ViewResult aView) {
	this.view = aView;
	addWindowListener(new WindowAdapter() {
	    public void windowClosing(final WindowEvent we) {
		setVisible(false);
	    }
	});
    }

    @Override
    public void paint(Graphics g) {
	Rectangle r = getBounds(), c = new Rectangle();
	r.x = BORDER;
	r.y = BORDER;
	r.width -= r.x + BORDER;
	r.height -= r.y + BORDER;

	c.x = r.x;
	c.y = r.y + r.height;
	c.width = r.width;
	c.height = r.height;

	g.setColor(Color.BLUE);
	g.drawLine(c.x, c.y, c.x + c.width, c.y);
	g.drawLine(c.x, r.y, c.x, r.y + r.height);

	double maxX = view.getParams().get(0).getNumber();
	double maxY = view.getParams().get(0).getResult();
	double scaleX;
	double scaleY;

	for (Parameters item : view.getParams()) {
	    if (item.getNumber() > maxX) {
		maxX = item.getNumber();
	    }
	    if (item.getResult() > maxY) {
		maxY = item.getResult();
	    }
	}
	g.drawString("+" + maxY, r.x, r.y);
	g.drawString("+" + maxX,
		c.x + c.width - g.getFontMetrics().stringWidth("+" + maxX),
		c.y);
	scaleX = c.width / maxX;
	scaleY = c.height / maxY;
	g.setColor(Color.RED);
	for (Parameters item : view.getParams()) {
	    g.drawOval(c.x + (int) (item.getNumber() * scaleX) - 5,
		    c.y - (int) (item.getResult() * scaleY) - 5, 10, 10);
	}
    }
}