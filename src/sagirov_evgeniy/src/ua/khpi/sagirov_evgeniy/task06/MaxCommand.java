package ua.khpi.sagirov_evgeniy.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.sagirov_evgeniy.task03.ViewResult;
import ua.khpi.sagirov_evgeniy.task05.Command;

/**
 * Finds maximum.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class MaxCommand implements Command {

    /** Result. */
    private int result = -1;

    /** Readiness of result. */
    private int progress = 0;

    /** Serves collection of {@linkplain Parameters}. */
    private ViewResult viewResult;

    /**
     * Gets the value of {@linkplain MaxCommand#result}.
     * 
     * @return value of {@linkplain MaxCommand#result}
     */
    public final int getResult() {
	return result;
    }

    /**
     * Gets the value of {@linkplain MaxCommand#viewResult}.
     * 
     * @return value of {@linkplain MaxCommand#viewResult}
     */
    public final ViewResult getViewResult() {
	return viewResult;
    }

    /**
     * Sets {@linkplain MaxCommand#viewResult} with new value.
     * 
     * @param aViewResult new value for {@linkplain MaxCommand#viewResult}
     */
    public final void setViewResult(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Initializes {@linkplain MaxCommand#viewResult} field.
     * 
     * @param aViewResult {@linkplain ViewResult} object
     */
    public MaxCommand(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Checks readiness of result.
     * 
     * @return false - result found, else - true
     */
    public final boolean running() {
	return progress < 100;
    }

    @Override
    public final void execute() {
	progress = 0;
	System.out.println("Max executed...");
	int size = viewResult.getParams().size();
	result = 0;
	for (int idx = 1; idx < size; idx++) {
	    if (viewResult.getParams().get(result).getNumber() < viewResult
		    .getParams().get(idx).getNumber()) {
		result = idx;
	    }
	    progress = idx * 100 / size;
	    if (idx % (size / 3) == 0) {
		System.out.println("Max " + progress + "%");
	    }
	    try {
		TimeUnit.MILLISECONDS.sleep(3000 / size);
	    } catch (InterruptedException e) {
		System.err.println(e);
	    }
	}
	System.out.println("Max done. Item #" + result + " found: "
		+ viewResult.getParams().get(result));
	progress = 100;
    }
}