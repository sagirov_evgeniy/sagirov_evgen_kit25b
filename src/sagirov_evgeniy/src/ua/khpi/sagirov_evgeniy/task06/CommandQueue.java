package ua.khpi.sagirov_evgeniy.task06;

import java.util.Vector;

import ua.khpi.sagirov_evgeniy.task05.Command;

/**
 * @author sagirov evgeniy
 * @version 1.0
 */
public class CommandQueue implements Queue {

    /** Tasks queue. */
    private Vector<Command> tasks;

    /** Whether waiting, or not. */
    private boolean waiting;

    /** Whether shutdown, or not. */
    private boolean shutdown;

    /** Sets shutdown flag. */
    public final void shutdown() {
	shutdown = true;
    }

    /**
     * Constructor.
     */
    public CommandQueue() {
	this.tasks = new Vector<Command>();
	this.waiting = false;
	new Thread(new Worker()).start();
    }

    @Override
    public final void put(final Command cmd) {
	tasks.add(cmd);
	if (waiting) {
	    synchronized (this) {
		notifyAll();
	    }
	}
    }

    @Override
    public final Command take() {
	if (tasks.isEmpty()) {
	    synchronized (this) {
		waiting = true;
		try {
		    wait();
		} catch (InterruptedException ie) {
		    waiting = false;
		}
	    }
	}
	return (Command) tasks.remove(0);
    }

    /**
     * Serves queue.
     * 
     * @author sagirov evgeniy
     * @version 1.0
     * @see Runnable
     */
    private class Worker implements Runnable {
	/**
	 * Takes ready to execute tasks from queue.
	 */
	public void run() {
	    while (!shutdown) {
		Command r = take();
		r.execute();
	    }
	}
    }

}
