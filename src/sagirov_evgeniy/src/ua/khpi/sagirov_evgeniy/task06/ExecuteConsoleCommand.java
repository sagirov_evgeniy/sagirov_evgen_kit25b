package ua.khpi.sagirov_evgeniy.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;
import ua.khpi.sagirov_evgeniy.task05.ConsoleCommand;

/**
 * @author sagirov evgeniy
 * @version 1.0
 */
public class ExecuteConsoleCommand implements ConsoleCommand {

    /**  */
    private View view;

    /**
     * Gets the value of {@linkplain ExecuteConsoleCommand#view}.
     * 
     * @return value of {@linkplain ExecuteConsoleCommand#view}
     */
    public final View getView() {
	return view;
    }

    /**
     * Sets {@linkplain ExecuteConsoleCommand#view} with new value.
     * 
     * @param aView new value for {@linkplain ExecuteConsoleCommand#view}
     */
    public final void setView(final View aView) {
	this.view = aView;
    }

    /**
     * Constructor.
     * 
     * @param aView
     */
    public ExecuteConsoleCommand(final View aView) {
	this.view = aView;
    }

    @Override
    public void execute() {
	CommandQueue queue1 = new CommandQueue();
	CommandQueue queue2 = new CommandQueue();
	/**
	 * ExecutorService exec1 = Executors.newSingleThreadExecutor();
	 * ExecutorService exec2 = Executors.newSingleThreadExecutor(); /
	 **/
	MaxCommand maxCommand = new MaxCommand((ViewResult) view);
	AvgCommand avgCommand = new AvgCommand((ViewResult) view);
	MinMaxCommand minMaxCommand = new MinMaxCommand((ViewResult) view);
	System.out.println("Execute all threads...");
	/**
	 * exec1.execute(minMaxCommand); exec2.execute(maxCommand);
	 * exec2.execute(avgCommand); /
	 **/
	queue1.put(minMaxCommand);
	queue2.put(maxCommand);
	queue2.put(avgCommand);
	/**/
	try {
	    while (avgCommand.running() || maxCommand.running()
		    || minMaxCommand.running()) {
		TimeUnit.MILLISECONDS.sleep(100);
	    }
	    /**
	     * exec1.shutdown(); exec2.shutdown(); /
	     **/
	    queue1.shutdown();
	    queue2.shutdown();
	    /**/
	    TimeUnit.SECONDS.sleep(1);
	} catch (InterruptedException e) {
	    System.err.println(e);
	}
	System.out.println("All done.");
    }

    @Override
    public final char getKey() {
	return 'e';
    }

    @Override
    public final String toString() {
	return "'e'xecute";
    }
}