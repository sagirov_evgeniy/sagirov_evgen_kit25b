package ua.khpi.sagirov_evgeniy.task06;

import ua.khpi.sagirov_evgeniy.task05.Command;

/**
 * Describes queue methods.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface Queue {

    /**
     * Adds new command to queue.
     * 
     * @param cmd command
     */
    void put(Command cmd);

    /**
     * Deletes command from queue.
     * 
     * @return command to delete
     */
    Command take();
}