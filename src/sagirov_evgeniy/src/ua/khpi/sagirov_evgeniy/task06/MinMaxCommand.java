package ua.khpi.sagirov_evgeniy.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;
import ua.khpi.sagirov_evgeniy.task05.Command;

/**
 * Find min and max.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class MinMaxCommand implements Command {

    /** Min of collection. */
    private int resultMin = -1;

    /** Max of collection. */
    private int resultMax = -1;

    /** Readiness of result. */
    private int progress = 0;

    /** Serves collection of {@linkplain Parameters}. */
    private ViewResult viewResult;

    /**
     * Gets the value of {@linkplain MinMaxCommand#viewResult}.
     * 
     * @return value of {@linkplain MinMaxCommand#viewResult}
     */
    public final ViewResult getViewResult() {
	return viewResult;
    }

    /**
     * Sets {@linkplain MinMaxCommand#viewResult} with new value.
     * 
     * @param aViewResult new value for {@linkplain MinMaxCommand#viewResult}
     */
    public final void setViewResult(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Constructor.
     * 
     * @param aViewResult {@linkplain ViewResult} object
     */
    public MinMaxCommand(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Gets the value of {@linkplain MinMaxCommand#resultMin}.
     * 
     * @return value of {@linkplain MinMaxCommand#resultMin}
     */
    public final int getResultMin() {
	return resultMin;
    }

    /**
     * Gets the value of {@linkplain MinMaxCommand#resultMax}.
     * 
     * @return value of {@linkplain MinMaxCommand#resultMax}
     */
    public final int getResultMax() {
	return resultMax;
    }

    /**
     * Checks readiness of result.
     * 
     * @return false - result found, else - true
     */
    public final boolean running() {
	return progress < 100;
    }

    @Override
    public final void execute() {
	progress = 0;
	System.out.println("MinMax executed...");
	int idx = 0, size = viewResult.getParams().size();
	for (Parameters param : viewResult.getParams()) {
	    if ((resultMax == -1) || (viewResult.getParams().get(resultMax)
		    .getResult() < param.getNumber())) {
		resultMax = idx;
	    }
	    if ((resultMin == -1) || (viewResult.getParams().get(resultMin)
		    .getResult() > param.getNumber())) {
		resultMin = idx;
	    }
	    idx++;
	    progress = idx * 100 / size;
	    if (idx % (size / 5) == 0) {
		System.out.println("MinMax " + progress + "%");
	    }
	    try {
		TimeUnit.MILLISECONDS.sleep(5000 / size);
	    } catch (InterruptedException e) {
		System.err.println(e);
	    }
	}
	System.out.print("MinMax done. ");
	if (resultMin > -1) {
	    System.out.print("Min #" + resultMin + " found: "
		    + viewResult.getParams().get(resultMin).getNumber());
	} else {
	    System.out.print("Min positive not found.");
	}
	if (resultMax > -1) {
	    System.out.println(" Max #" + resultMax + " found: "
		    + viewResult.getParams().get(resultMax).getNumber());
	} else {
	    System.out.println(" Max negative item not found.");
	}
	progress = 100;
    }
}