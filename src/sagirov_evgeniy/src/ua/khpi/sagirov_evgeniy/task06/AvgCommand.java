package ua.khpi.sagirov_evgeniy.task06;

import java.util.concurrent.TimeUnit;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;
import ua.khpi.sagirov_evgeniy.task05.Command;

/**
 * Finds average result.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class AvgCommand implements Command {

    /** Stores result of operation. */
    private int result = 0;

    /** Readiness of result. */
    private int progress = 0;

    /** Serves collection of {@linkplain Parameters}. */
    private ViewResult viewResult;

    /**
     * Gets the value of {@linkplain AvgCommand#viewResult}.
     * 
     * @return value of {@linkplain AvgCommand#viewResult}
     */
    public final ViewResult getViewResult() {
	return viewResult;
    }

    /**
     * Sets {@linkplain AvgCommand#viewResult} with new value.
     * 
     * @param aViewResult new value for {@linkplain AvgCommand#viewResult}
     */
    public final void setViewResult(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Initializes field {@linkplain MaxCommand#viewResult}.
     * 
     * @param aViewResult {@linkplain ViewResult} object
     */
    public AvgCommand(final ViewResult aViewResult) {
	this.viewResult = aViewResult;
    }

    /**
     * Gets the value of {@linkplain AvgCommand#result}.
     * 
     * @return value of {@linkplain AvgCommand#result}
     */
    public final int getResult() {
	return result;
    }

    /**
     * Checks readiness of result.
     * 
     * @return false - result found, else - true
     */
    public final boolean running() {
	return progress < 100;
    }

    @Override
    public final void execute() {
	progress = 0;
	System.out.println("Average executed...");
	result = 0;
	int idx = 1, size = viewResult.getParams().size();
	for (Parameters item : viewResult.getParams()) {
	    result += item.getNumber();
	    progress = idx * 100 / size;
	    if (idx++ % (size / 2) == 0) {
		System.out.println("Average " + progress + "%");
	    }
	    try {
		TimeUnit.MILLISECONDS.sleep(2000 / size);
	    } catch (InterruptedException e) {
		System.err.println(e);
	    }
	}
	result /= size;
	System.out.println("Average done. Result = " + result);
	progress = 100;
    }
}