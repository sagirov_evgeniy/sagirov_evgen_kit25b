package ua.khpi.sagirov_evgeniy.task06;

import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.ViewableResult;
import ua.khpi.sagirov_evgeniy.task05.ChangeConsoleCommand;
import ua.khpi.sagirov_evgeniy.task05.GenerateConsoleCommand;
import ua.khpi.sagirov_evgeniy.task05.Menu;
import ua.khpi.sagirov_evgeniy.task05.ViewConsoleCommand;

/**
 * Calculates and shows results.
 * 
 * @author sagirov evgeniy
 * @version 5.0
 */
public final class Demo {

    /** Hidden constructor. */
    private Demo() {
    }

    /**  */
    private View view = new ViewableResult().getView();

    /**  */
    private Menu menu = new Menu();

    /**
     * 
     */
    public void run() {
	menu.add(new ViewConsoleCommand(view));
	menu.add(new GenerateConsoleCommand(view));
	menu.add(new ChangeConsoleCommand(view));
	menu.add(new ExecuteConsoleCommand(view));
	menu.execute();
    }
    
    /**
     * Entry point of console program.
     * 
     * @param args array of incoming arguments from console
     */
    public static void main(final String[] args) {
	Demo demo = new Demo();
	demo.run();
    }
}