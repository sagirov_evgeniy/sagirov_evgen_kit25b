package ua.khpi.sagirov_evgeniy.task01;

/**
 * Print command-line parameters.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public abstract class Main {

    /**
     * Program entry point.
     * 
     * @param args command-line parameters list
     */
    public static void main(final String[] args) {
	System.out.println("---Start---");
	for (String s : args) {
	    System.out.println(s);
	}
	System.out.println("---End---");
    }
}