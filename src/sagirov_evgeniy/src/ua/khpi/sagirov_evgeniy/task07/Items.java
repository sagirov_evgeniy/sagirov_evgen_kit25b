package ua.khpi.sagirov_evgeniy.task07;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Collection.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class Items extends Observable implements Iterable<Item> {

    /** Event state. */
    public static final String ITEMS_CHANGED = "ITEMS_CHANGED";

    /** Event state. */
    public static final String ITEMS_EMPTY = "ITEMS_EMPTY";

    /** Event state. */
    public static final String ITEMS_REMOVED = "ITEMS_REMOVED";

    /** Collection of {@linkplain Item} objects. */
    private List<Item> items = new ArrayList<Item>();

    /**
     * Adds object to collection and notifies listeners.
     * 
     * @param item item to add to collection
     */
    public final void add(final Item item) {
	items.add(item);
	if (item.getData().isEmpty()) {
	    call(ITEMS_EMPTY);
	} else {
	    call(ITEMS_CHANGED);
	}
    }

    /**
     * Adds event to collection.
     * 
     * @param s {@linkplain Item#Item(String)} argument
     */
    public final void add(final String s) {
	add(new Item(s));
    }

    /**
     * Adds few items to collection.
     * 
     * @param n number of new objects
     */
    public final void add(int n) {
	if (n > 0) {
	    while (n-- > 0) {
		items.add(new Item(""));
		call(ITEMS_EMPTY);
	    }
	}
    }

    /**
     * Deletes object from collection and notifies.
     * 
     * @param item object to delete
     */
    public final void delete(final Item item) {
	if (item != null) {
	    items.remove(item);
	    call(ITEMS_REMOVED);
	}
    }

    /**
     * Deletes object from collection and notifies.
     * 
     * @param index index of object to delete
     */
    public final void delete(final int index) {
	if ((index >= 0) && (index < items.size())) {
	    items.remove(index);
	    call(ITEMS_REMOVED);
	}
    }

    /**
     * Gets the value of {@linkplain Items#items}.
     * 
     * @return value of {@linkplain Items#items}
     */
    public final List<Item> getItems() {
	return items;
    }

    @Override
    public final Iterator<Item> iterator() {
	return items.iterator();
    }
}