package ua.khpi.sagirov_evgeniy.task07;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Superclass for observer, uses annotation to recognize methods.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public abstract class AnnotatedObserver implements Observer {

    /** Collection of handlers. */
    private Map<Object, Method> handlers = new HashMap<Object, Method>();

    /**
     * Constructor. Fills {@linkplain AnnotatedObserver#handlers} with methods
     * links, marked with {@linkplain Event} annotation.
     */
    public AnnotatedObserver() {
	for (Method m : this.getClass().getMethods()) {
	    if (m.isAnnotationPresent(Event.class)) {
		handlers.put(m.getAnnotation(Event.class).value(), m);
	    }
	}
    }

    @Override
    public final void handleEvent(final Observable observable,
	    final Object event) {
	Method m = handlers.get(event);
	try {
	    if (m != null) {
		m.invoke(this, observable);
	    }
	} catch (Exception e) {
	    System.err.println(e);
	}
    }
}
