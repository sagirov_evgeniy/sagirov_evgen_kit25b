package ua.khpi.sagirov_evgeniy.task07;

/**
 * Collection element.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public class Item implements Comparable<Item> {

    /** Information field. */
    private String data;

    /**
     * Constructor. Initializes {@linkplain Item#data}.
     * 
     * @param aData {@linkplain Item#data} value
     */
    public Item(final String aData) {
	this.data = aData;
    }

    /**
     * Gets the value of {@linkplain Item#data}.
     * 
     * @return value of {@linkplain Item#data}
     */
    public final String getData() {
	return data;
    }

    /**
     * Sets {@linkplain Item#data} with new value.
     * 
     * @param aData new value for {@linkplain Item#data}
     */
    public final void setData(final String aData) {
	this.data = aData;
    }

    @Override
    public final int compareTo(final Item item) {
	return data.compareTo(item.data);
    }

    @Override
    public final String toString() {
	return data;
    }
}