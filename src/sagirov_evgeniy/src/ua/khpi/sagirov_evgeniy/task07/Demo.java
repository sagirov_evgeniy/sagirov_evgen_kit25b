package ua.khpi.sagirov_evgeniy.task07;

import ua.khpi.sagirov_evgeniy.task05.ConsoleCommand;
import ua.khpi.sagirov_evgeniy.task05.Menu;

/**
 * Dialogue woth user.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public final class Demo {

    /** Hidden constructor. */
    private Demo() {
    }

    /**
     * COnsole comman to create anonymous commands.
     * 
     * @author xone
     * @see ConsoleCommand
     */
    abstract class ConsoleCmd implements ConsoleCommand {
	/** Collection of {@linkplain Items} objects. */
	protected Items items;

	/** Command title. */
	private String name;

	/** Hotkey symbol. */
	private char key;

	/**
	 * Initializes console command fields.
	 * 
	 * @param aItems {@linkplain ConsoleCmd#items}
	 * @param aName {@linkplain ConsoleCmd#name}
	 * @param aKey {@linkplain ConsoleCmd#key}
	 */
	ConsoleCmd(final Items aItems, final String aName, final char aKey) {
	    this.items = aItems;
	    this.name = aName;
	    this.key = aKey;
	}

	@Override
	public char getKey() {
	    return key;
	}

	@Override
	public String toString() {
	    return name;
	}
    }

    /**
     * Connects observers with listeners.
     */
    public void run() {
	Items items = new Items();
	ItemsGenerator generator = new ItemsGenerator();
	ItemsSorter sorter = new ItemsSorter();
	items.addObserver(generator);
	items.addObserver(sorter);
	Menu menu = new Menu();
	menu.add(new ConsoleCmd(items, "'v'iew", 'v') {
	    @Override
	    public void execute() {
		System.out.println(items.getItems());
	    }
	});
	menu.add(new ConsoleCmd(items, "'a'dd", 'a') {
	    @Override
	    public void execute() {
		items.add("");
	    }
	});
	menu.add(new ConsoleCmd(items, "'d'el", 'd') {
	    @Override
	    public void execute() {
		items.delete((int) Math
			.round(Math.random() * (items.getItems().size() - 1)));
	    }
	});
	menu.execute();
    }

    /**
     * Entry point of cosole program.
     * 
     * @param args array of incoming rguments
     */
    public static void main(final String[] args) {
	new Demo().run();
    }
}