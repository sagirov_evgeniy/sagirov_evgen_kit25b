package ua.khpi.sagirov_evgeniy.task07;

import java.util.HashSet;
import java.util.Set;

/**
 * Shows methods of connection between observer and abservable objects.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public abstract class Observable {
    /**
     * Observers.
     * 
     * @see Observer
     */
    private Set<Observer> observers = new HashSet<Observer>();

    /**
     * Adds observer.
     * 
     * @param observer observer object
     */
    public final void addObserver(final Observer observer) {
	observers.add(observer);
    }

    /**
     * Deletes observer.
     * 
     * @param observer observer object
     */
    public final void deleteObserver(final Observer observer) {
	observers.remove(observer);
    }

    /**
     * Notifies listeners about event.
     * 
     * @param event event information
     */
    public final void call(final Object event) {
	for (Observer observer : observers) {
	    observer.handleEvent(this, event);
	}
    }
}