package ua.khpi.sagirov_evgeniy.task07;

/**
 * Shows connection between observable and observer.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface Observer {

    /**
     * Calls for every observer.
     * 
     * @param observable observable object
     * @param event event information
     */
    void handleEvent(Observable observable, Object event);
}