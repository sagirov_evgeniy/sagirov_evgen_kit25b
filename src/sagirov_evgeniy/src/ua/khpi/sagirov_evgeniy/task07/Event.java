package ua.khpi.sagirov_evgeniy.task07;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Runtime annotation to set events to observers methods.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Event {
    String value();
}