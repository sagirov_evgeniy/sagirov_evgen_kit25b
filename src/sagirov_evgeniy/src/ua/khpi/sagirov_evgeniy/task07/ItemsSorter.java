package ua.khpi.sagirov_evgeniy.task07;

import java.util.Collections;

/**
 * Observer.
 * 
 * @author sagirov evgeniy
 * @see AnnotatedObserver
 * @see Event
 */
public class ItemsSorter extends AnnotatedObserver {

    /** Evente state. */
    public static final String ITEMS_SORTED = "ITEMS_SORTED";

    /**
     * Notifies listeners.
     * 
     * @param observable observable {@linkplain Items} object
     */
    @Event(Items.ITEMS_CHANGED)
    public void itemsChanged(Items observable) {
	Collections.sort(observable.getItems());
	observable.call(ITEMS_SORTED);
    }

    /**
     * Notifies listeners.
     * 
     * @param observable observable {@linkplain Items} object
     */
    @Event(ITEMS_SORTED)
    public void itemsSorted(Items observable) {
	System.out.println(observable.getItems());
    }

    /**
     * Notifies listeners.
     * 
     * @param observable observable {@linkplain Items} object
     */
    @Event(Items.ITEMS_REMOVED)
    public void itemsRemoved(Items observable) {
	System.out.println(observable.getItems());
    }
}