package ua.khpi.sagirov_evgeniy.task03;

import java.io.IOException;

/**
 * Product.<br>
 * Interface of "factoried" objects<br>
 * Declares methods of viewable objects
 *
 * @author sagirov evgeniy
 * @version 1.0
 */
public interface View {

    /** Shows main part. */
    void viewBody();

    /** Shows ending. */
    void viewFooter();

    /** Shows header. */
    void viewHeader();

    /** Initialization. */
    void viewInit();

    /**
     * Restores data.
     * 
     * @throws Exception
     */
    void viewRestore() throws Exception;

    /**
     * Saves data.
     * 
     * @throws IOException
     */
    void viewSave() throws IOException;

    /** Shows whole object. */
    void viewShow();

    /** Solving. */
    void viewSolve();

    /** Solving. */
    boolean viewUndo();
}
