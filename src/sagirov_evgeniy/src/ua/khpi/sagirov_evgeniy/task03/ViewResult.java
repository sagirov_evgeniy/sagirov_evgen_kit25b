package ua.khpi.sagirov_evgeniy.task03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task05.ParameterStateManager;

/**
 * ConcreteProduct<br>
 * Calculates tetrades, saves and shows results.
 *
 * @author sagirov evgeniy
 * @version 1.0
 * @see View
 */
public class ViewResult implements View {

    /** Filename fo serialization. */
    private static final String FNAME = "params.out";

    /** Default number of calculations. */
    private static final int DEFAULT_NUM = 10;

    /** Collection of arguments and results. */
    private ArrayList<Parameters> params;

    /**
     * Calls {@linkplain ViewResult#ViewResult(int)} with
     * {@linkplain ViewResult#DEFAULT_NUM} as an argument.
     */
    public ViewResult() {
	this(DEFAULT_NUM);
    }

    /**
     * Initializes collection {@linkplain ViewResult#params}.
     *
     * @param n start collection length
     */
    public ViewResult(final int n) {
	params = new ArrayList<Parameters>();
	for (int ctr = 0; ctr < n; ctr++) {
	    params.add(new Parameters());
	}
    }

    /**
     * Calculates the amount of full tetrades.
     *
     * @param number - number to calculate
     * @return result of calculations
     */
    private final int calc(final int number) {
	final String binaryString = Integer.toBinaryString(number);
	return binaryString.length() / Parameters.TETRADE;
    }

    /**
     * Gets {@linkplain ViewResult#params}.
     *
     * @return current value of {@linkplain ArrayList} address
     */
    public final ArrayList<Parameters> getParams() {
	return params;
    }

    /**
     * Calculates tetrades and saves result in collection
     * {@linkplain ViewResult#params}.
     *
     * @param stepNumber step of number incrementing
     */
    public void init(final int stepNumber) {
	int number = 0;
	for (final Parameters param : params) {
	    param.setNumber(number);
	    param.setResult(calc(number));
	    number += stepNumber;
	}
    }

    @Override
    public void viewBody() {
	for (final Parameters param : params) {
	    System.out.println(param);
	}
	System.out.println();
    }

    @Override
    public void viewFooter() {
	System.out.println("End.");
    }

    @Override
    public void viewHeader() {
	System.out.println("Results:");

    }

    @Override
    public void viewInit() {
	final int maxRandomValue = 50;
	init(new Random().nextInt(maxRandomValue));
    }

    @SuppressWarnings("unchecked")
    @Override
    public final void viewRestore() throws Exception {
	final ObjectInputStream ois = new ObjectInputStream(
		new FileInputStream(FNAME));
	params = (ArrayList<Parameters>) ois.readObject();
	ois.close();
    }

    @Override
    public final void viewSave() throws IOException {
	final ObjectOutputStream oos = new ObjectOutputStream(
		new FileOutputStream(FNAME));
	oos.writeObject(params);
	oos.flush();
	oos.close();
    }

    @Override
    public void viewShow() {
	viewHeader();
	viewBody();
	viewFooter();
    }

    @Override
    public final void viewSolve() {
	for (Parameters param : params) {
	    param.setResult(calc(param.getNumber()));
	}
    }

    @Override
    public boolean viewUndo() {
	ParameterStateManager psm = ParameterStateManager.getInstance();
	if (psm.getLength() > 0) {
	    params = new ArrayList<Parameters>(psm.getPrevious());
	    return true;
	}
	return false;
    }
}