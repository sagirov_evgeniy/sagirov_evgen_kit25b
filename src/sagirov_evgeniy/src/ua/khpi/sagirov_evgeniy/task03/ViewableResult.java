package ua.khpi.sagirov_evgeniy.task03;

/**
 * ConcreteCreator<br>
 * Implements method "factoring" objects.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 * @see Viewable
 * @see ViewableResult#getView()
 */
public class ViewableResult implements Viewable {
    @Override
    public View getView() {
	return new ViewResult();
    }
}