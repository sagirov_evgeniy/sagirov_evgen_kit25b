package ua.khpi.sagirov_evgeniy.task03;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Calculates and show results.<br>
 * 
 * @author sagirov evgeniy
 * @version 2.0
 */
public class Demo {

    /**
     * Object, implements interface {@linkplain View}; serves collection of
     * {@linkplain ua.khpi.sagirov_evgeniy.task02.Parameters} object.
     */
    private View view;

    /**
     * Initializes {@linkplain Demo#view}.
     * 
     * @param aView {@linkplain View} object
     */
    public Demo(final View aView) {
	this.view = aView;
    }

    /** Shows menu. */
    protected void menu() {
	String s = null;
	BufferedReader in = new BufferedReader(
		new InputStreamReader(System.in));
	do {
	    do {
		System.out.println("Enter command...");
		System.out.print(
			"'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore:");
		try {
		    s = in.readLine();
		} catch (IOException e) {
		    System.out.println("Error: " + e);
		    System.exit(0);
		}
	    } while (s.length() != 1);
	    switch (s.charAt(0)) {
	    case 'q':
		System.out.println("Exit.");
		break;
	    case 'v':
		System.out.println("View current.");
		view.viewShow();
		break;
	    case 'g':
		System.out.println("Random generation.");
		view.viewInit();
		view.viewShow();
		break;
	    case 's':
		System.out.println("Save current.");
		try {
		    view.viewSave();
		} catch (IOException e) {
		    System.out.println("Serialization error: " + e);
		}
		view.viewShow();
		break;
	    case 'r':
		System.out.println("Restore last saved.");
		try {
		    view.viewRestore();
		} catch (Exception e) {
		    System.out.println("Deserialization error: " + e);
		}
		view.viewShow();
		break;
	    default:
		System.out.println("Wrong command.");
		break;
	    }
	} while (s.charAt(0) != 'q');
    }

    /**
     * Entry point of console programm.
     * 
     * @param args array of command line arguments
     */
    public static void main(final String[] args) {
	Demo demo = new Demo(new ViewableResult().getView());
	demo.menu();
    }
}