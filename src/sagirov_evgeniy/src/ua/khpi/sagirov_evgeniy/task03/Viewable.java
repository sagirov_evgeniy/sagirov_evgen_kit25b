package ua.khpi.sagirov_evgeniy.task03;

/**
 * Creator. <br>
 * Declares method, which "factory" objects
 * 
 * @author sagirov evgeniy
 * @version 1.0
 * @see Viewable#getView()
 */
public interface Viewable {

    /**
     * Creates object, which implements {@linkplain View}.
     * 
     * @return object, implements {@linkplain View}
     */
    View getView();
}
