package ua.khpi.sagirov_evgeniy.task02;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Demonstrates serialization/deserialization of object.
 * 
 * @author sagirov evgeniy
 * @version 1.1
 */
public class Demo {

    /** Object of class {@linkplain Solver}. */
    private Solver solver = new Solver();

    /** Shows menu. */
    private void menu() {
	String s = null;
	final int maxRandomValue = 5000;
	BufferedReader in = new BufferedReader(
		new InputStreamReader(System.in));
	do {
	    do {
		System.out.println("Enter command...");
		System.out.print(
			"'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore, 'c'alculate: ");
		try {
		    s = in.readLine();
		} catch (IOException e) {
		    System.out.println("Error: " + e);
		    System.exit(0);
		}
	    } while (s.length() != 1);
	    switch (s.charAt(0)) {
	    case 'q':
		System.out.println("Exit.");
		break;
	    case 'v':
		System.out.println("View current.");
		solver.show();
		break;
	    case 'g':
		System.out.println("Random generation.");
		solver.solveAndSave(new Random().nextInt(maxRandomValue));
		solver.show();
		break;
	    case 's':
		System.out.println("Save current.");
		try {
		    solver.save();
		} catch (IOException e) {
		    System.out.println("Serialization error: " + e);
		}
		solver.show();
		break;
	    case 'r':
		System.out.println("Restore last saved.");
		try {
		    solver.restore();
		} catch (Exception e) {
		    System.out.println("Deserialization error: " + e);
		}
		solver.show();
		break;
	    case 'c':
		System.out.println("Calculate result:");
		solver.solveAndSave(solver.getParams().getNumber());
		solver.show();
		break;
	    default:
		System.out.println("Wrong command.");
		break;
	    }
	} while (s.charAt(0) != 'q');
    }

    /**
     * Entry point of console programm.
     * 
     * @param args array of command line arguments
     */
    public static void main(final String[] args) {
	Demo demo = new Demo();
	demo.menu();
    }
}