package ua.khpi.sagirov_evgeniy.task02;

import java.io.Serializable;

/**
 * Stores parameters of calculations and it's result.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public final class Parameters implements Serializable {

    /** Version of class. */
    private static final long serialVersionUID = 1L;

    /** Tetrade value. */
    public static final int TETRADE = 4;

    /** Numeric value to count it's full tetrades. */
    private int number;

    /** Result of calculations. */
    private transient int result;

    /** Default constructor. */
    public Parameters() {
    }

    /**
     * Copy constructor.
     * 
     * @param source object to copy from
     */
    public Parameters(final Parameters source) {
	this.number = source.number;
	this.result = source.result;
    }

    /**
     * Gets value of {@linkplain Parameters#result}.
     * 
     * @return {@linkplain Parameters#result} value
     */
    public int getResult() {
	return result;
    }

    /**
     * Sets a new value to {@linkplain Parameters#result}.
     * 
     * @param aResult - new value of {@linkplain Parameters#result}
     */
    public void setResult(final int aResult) {
	this.result = aResult;
    }

    /**
     * Gets value of {@linkplain Parameters#number}.
     * 
     * @return the value of {@linkplain Parameters#number}
     */
    public int getNumber() {
	return number;
    }

    /**
     * Sets new value of {@linkplain Parameters#number}.
     * 
     * @param aNumber new value of {@linkplain Parameters#number}
     */
    public void setNumber(final int aNumber) {
	this.number = aNumber;
    }

    /** Represents data of object into a string. */
    @Override
    public String toString() {
	return "Number: " + number + ". Full tetrades: " + result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	Parameters other = (Parameters) obj;
	if (number != other.number) {
	    return false;
	}
	return true;
    }

}