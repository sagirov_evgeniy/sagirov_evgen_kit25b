package ua.khpi.sagirov_evgeniy.task02;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Solves task main objective.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 */
public final class Solver {

    /** Filename for serialization. */
    private static final String FNAME = "params.out";

    /** Parameters of calculation. */
    private Parameters params;

    /**
     * Initialize {@linkplain Solver#params}.
     * 
     * @param aParams incoming parameters
     */
    Solver(final Parameters aParams) {
	params = aParams;
    }

    /** Initialize {@linkplain Solver#params}. */
    Solver() {
	params = new Parameters();
    }

    /**
     * Gets value of {@linkplain Solver#params}.
     * 
     * @return current value of adress to {@linkplain Parameters}
     */
    public Parameters getParams() {
	return params;
    }

    /**
     * Sets value of {@linkplain Solver#params}.
     * 
     * @param aParams - new value of adress to object of {@linkplain Parameters}
     */
    public void setParams(final Parameters aParams) {
	this.params = aParams;
    }

    /**
     * Calculates the amount of full tetrades.
     * 
     * @param number - number to calculate
     * @return result of calculations
     */
    public int solve(final int number) {
	String binaryString = Integer.toBinaryString(number);
	return binaryString.length() / Parameters.TETRADE;
    }

    /**
     * Calculates the amount of full tetrades and saves it to
     * {@linkplain Solver#params}.
     * 
     * @param number - number to calculate
     * @return resulot of calculations
     */
    public int solveAndSave(final int number) {
	params.setNumber(number);
	params.setResult(solve(number));
	return params.getResult();
    }

    /**
     * Saves {@linkplain Solver#params} to file {@linkplain Solver#FNAME}.
     * 
     * @throws IOException
     */
    public void save() throws IOException {
	ObjectOutputStream oos = new ObjectOutputStream(
		new FileOutputStream(FNAME));
	oos.writeObject(params);
	oos.flush();
	oos.close();
    }

    /**
     * Restores {@linkplain Solver#params} from file {@linkplain Solver#FNAME}.
     * 
     * @throws Exception
     */
    public void restore() throws Exception {
	ObjectInputStream ois = new ObjectInputStream(
		new FileInputStream(FNAME));
	params = (Parameters) ois.readObject();
	ois.close();
    }

    /**
     * Prints calculations result.
     */
    public void show() {
	System.out.println(params);
    }
}