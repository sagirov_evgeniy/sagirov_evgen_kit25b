package ua.khpi.sagirov_evgeniy.task04;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ua.khpi.sagirov_evgeniy.task03.View;
import ua.khpi.sagirov_evgeniy.task03.ViewableResult;

/**
 * ConcreteCreator.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 * @see ViewableResult
 * @see ViewableTable#getView()
 */
public class ViewableTable extends ViewableResult {

    /** Creates viewable object {@linkplain ViewTable}. */
    @Override
    public View getView() {
	int width = 0;
	BufferedReader in = new BufferedReader(
		new InputStreamReader(System.in));
	System.out.println("Enter table width:");
	try {
	    width = Integer.parseInt(in.readLine());
	} catch (NumberFormatException e) {
	    System.out.println(e.getMessage());
	} catch (IOException e) {
	    System.out.println(e.getMessage());
	}
	return new ViewTable(width);
    }
}
