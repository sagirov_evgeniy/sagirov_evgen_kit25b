package ua.khpi.sagirov_evgeniy.task04;

import java.util.Formatter;

import ua.khpi.sagirov_evgeniy.task02.Parameters;
import ua.khpi.sagirov_evgeniy.task03.ViewResult;

/**
 * ConcreteProduct.Shows data in table format.
 * 
 * @author sagirov evgeniy
 * @version 1.0
 * @see ViewResult
 */
public class ViewTable extends ViewResult {
    /** Default table width. */
    private static final int DEFAULT_WIDTH = 20;

    /** Current table width. */
    private int width;

    /**
     * Sets {@linkplain ViewTable#width} with
     * {@linkplain ViewTable#DEFAULT_WIDTH} value. Call super class constructor
     */
    public ViewTable() {
	width = DEFAULT_WIDTH;
    }

    /**
     * Sets {@linkplain ViewTable#width} with <b>width</b>.
     * 
     * @param aWidth sets table width
     */
    public ViewTable(final int aWidth) {
	this.width = aWidth;
    }

    /**
     * Sets {@linkplain ViewTable#width} width value <b>width</b>. Calls
     * superclass constructor {@linkplain ViewResult#ViewResult()}
     * 
     * @param aWidth sets table width
     * @param n collection length
     */
    public ViewTable(final int aWidth, final int n) {
	super(n);
	this.width = aWidth;
    }

    /**
     * Gets value of {@linkplain ViewTable#width}.
     * 
     * @return {@linkplain ViewTable#width} value
     */
    public final int getWidth() {
	return width;
    }

    /**
     * Sets value of {@linkplain ViewTable#width}.
     * 
     * @param aWidth new value for {@linkplain ViewTable#width}
     */
    public final void setWidth(final int aWidth) {
	this.width = aWidth;
    }

    /** Shows outter line. */
    private void outLine() {
	for (int i = width; i > 0; i--) {
	    System.out.print('-');
	}
    }

    /** Shows outter line and blank line. */
    private void outLineLn() {
	outLine();
	System.out.println();
    }

    /** Prints header. */
    private void outHeader() {
	final int spacing = 3;
	Formatter fmt = new Formatter();
	fmt.format("%s%d%s%2$d%s", "%", (width - spacing) / 2, "s | %", "s\n");
	System.out.printf(fmt.toString(), "Number    ", "Result    ");
	fmt.close();
    }

    /** Prints main info. */
    private void outBody() {
	final int spacing = 3;
	Formatter fmt = new Formatter();
	fmt.format("%s%d%s%2$d%s", "%", (width - spacing) / 2, "s | %", "s\n");
	for (Parameters param : getParams()) {
	    System.out.printf(fmt.toString(), param.getNumber(),
		    param.getResult());
	}
	fmt.close();
    }

    /**
     * Initializes view with width.
     * 
     * @param aWidth new value of width
     */
    public final void init(final byte aWidth) {
	this.width = aWidth;
	viewInit();
    }

    /**
     * Initializes width.
     * 
     * @param aWidth new value if {@linkplain ViewTable#width}
     * @param aStep new incrementing step
     */
    public final void init(final int aWidth, final int aStep) {
	this.width = aWidth;
	init(aStep);
    }

    /**
     * Initializes step of incrementing.
     * 
     * @param aStep new incrementing step
     */
    public final void init(final int aStep) {
	System.out.println("Initialization...");
	super.init(aStep);
	System.out.println("done.");
    }

    @Override
    public final void viewHeader() {
	outHeader();
	outLineLn();
    }

    @Override
    public final void viewBody() {
	outBody();
    }

    @Override
    public final void viewFooter() {
	outLineLn();
    }

}
