package ua.khpi.sagirov_evgeniy.task04;

import ua.khpi.sagirov_evgeniy.task03.View;

/**
 * Calculates and show results.<br>
 * 
 * @author sagirov evgeniy
 * @version 3.0
 */
public class Demo extends ua.khpi.sagirov_evgeniy.task03.Demo {

    /**
     * Initializes {@linkplain ua.khpi.sagirov_evgeniy.task03.Demo#view view}.
     * 
     * @param aView value to set field <b>view</b>}
     */
    public Demo(final View aView) {
	super(aView);
    }

    /**
     * Entry point of console programm.
     * 
     * @param args array of command line arguments
     */
    public static void main(final String[] args) {
	Demo demo = new Demo(new ViewableTable().getView());
	demo.menu();
    }
}