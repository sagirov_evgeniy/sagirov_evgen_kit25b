## [Інженерія програмного забезпечення. Частина перша: C++](https://www.assembla.com/spaces/khpi-se/subversion/source/HEAD)

### [Звіти та проекти до лабораторних робіт з С++](https://www.assembla.com/spaces/kit23a-2015-se/subversion/source/HEAD/zhmaiev)

## [Інженерія програмного забезпечення. Частина друга: Java](https://gitlab.com/khpi-se-java/info)

### Звіти та проекти до лабораторних робіт з Java:

| № | Тема | Звіт |
| --------------------- | ---- | ---- |
| 1 | Ознайомлення з системою контролю версій Git | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task01) |
| 2 | Класи та об'єкти. Агрегування. Серіалізація | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task02) |
| 3 | Спадкування. Інтерфейси. Колекції пакету java.util | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task03) |
| 4 | Поліморфізм. Форматований вивід | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task04) |
| 5 | Обробка колекцій. Шаблони Singletone, Command | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task05) |
| 6 | Паралельне виконання. Шаблон Worker Thread | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task06) |
| 7 | Анотація. Рефлексія. Шаблон Observer | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task07) |
| 8 | Використання графічних примытивів. Abstract Windows Toolkit | [Перейти до звіту](https://gitlab.com/khpi-se-kit23a/zhmaiev_anatolii/wikis/task08) |